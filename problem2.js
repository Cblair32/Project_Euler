function sumOfEvenFibNum(array){
  var sum = 0;
  for(var i = 0; i<array.length;i++){
    if(array[i]%2===0){
      sum+=array[i];
    }
  }
  return sum;
}
var fibonacci_series = function (max){
  var fib = [];
  fib[0] = 0;
  fib[1] = 1;
  for(var i=2; i<=max; i++){
    fib[i] = fib[i-2] + fib[i-1];
    if(fib[i]>=4000000){
      return fib;
    }
  }
  return fib;
}
var series = fibonacci_series(100);
console.log(sumOfEvenFibNum(series))
//console.log(series);