function isNumber (value) {
    return (value instanceof Number) || (typeof value == 'number');
}

function isInteger(value) {
  return (value === Math.round(value));
}

function gcd(a, b) {
  if (isNumber(a) && isNumber(b)) {
    if (!isInteger(a) || !isInteger(b)) {
      throw new Error('Parameters in function gcd must be integer numbers');
    }
    while (b != 0) {
     var r = a % b;
      a = b;
      b = r;
    }
    return (a < 0) ? -a : a;
  }
}

function lcm(a, b) {
  if (isNumber(a) && isNumber(b)) {
    if (!isInteger(a) || !isInteger(b)) {
      throw new Error('Parameters in function lcm must be integer numbers');
    }
    return (Math.abs(a) / gcd(a, b)) * Math.abs(b);
  }
}

function smallestMultiple(endOfRange) {
  if (isNumber(endOfRange)) {
    if (endOfRange <= 0) {
      throw new Error('Parameter in function smallestMultiple must be greater than zero');
    }
    if (!isInteger(endOfRange)) {
      throw new Error('Parameter in function smallestMultiple must be a positive integer');
    }
    var a = 1;
    for (var i = 1; i <= endOfRange; i++) {
      a = lcm(a, i);
    }
    return a;
  }
}

console.log(smallestMultiple(20));