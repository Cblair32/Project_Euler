function reverse_a_number(n)
{
	n = n + "";
	return n.split("").reverse().join("");
}

function isPalindrome(num){
  var temp = reverse_a_number(num);
  return num ==temp;
}

function palindromic(){
  var palindromes = [];
  for(var i = 999; i>99; i--){
    for(var j = 999; j>99; j--){
      var test = i*j;
      if(isPalindrome(test)){
        palindromes.push(test);
      }
    }
  }
  return palindromes;
}

function getMaxOfArray(numArray) {
  return Math.max.apply(null, numArray);
}

console.log(getMaxOfArray(palindromic()))
